paver.tests package
===================

Submodules
----------

paver.tests.other_pavement module
---------------------------------

.. automodule:: paver.tests.other_pavement
    :members:
    :undoc-members:
    :show-inheritance:

paver.tests.test_doctools module
--------------------------------

.. automodule:: paver.tests.test_doctools
    :members:
    :undoc-members:
    :show-inheritance:

paver.tests.test_git module
---------------------------

.. automodule:: paver.tests.test_git
    :members:
    :undoc-members:
    :show-inheritance:

paver.tests.test_options module
-------------------------------

.. automodule:: paver.tests.test_options
    :members:
    :undoc-members:
    :show-inheritance:

paver.tests.test_path module
----------------------------

.. automodule:: paver.tests.test_path
    :members:
    :undoc-members:
    :show-inheritance:

paver.tests.test_setuputils module
----------------------------------

.. automodule:: paver.tests.test_setuputils
    :members:
    :undoc-members:
    :show-inheritance:

paver.tests.test_shell module
-----------------------------

.. automodule:: paver.tests.test_shell
    :members:
    :undoc-members:
    :show-inheritance:

paver.tests.test_svn module
---------------------------

.. automodule:: paver.tests.test_svn
    :members:
    :undoc-members:
    :show-inheritance:

paver.tests.test_tasks module
-----------------------------

.. automodule:: paver.tests.test_tasks
    :members:
    :undoc-members:
    :show-inheritance:

paver.tests.utils module
------------------------

.. automodule:: paver.tests.utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: paver.tests
    :members:
    :undoc-members:
    :show-inheritance:
